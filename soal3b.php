
 <?php
 $status = 0 ;
 ?>
<html>
    <head>
        <title>Soal 3</title>
        <style>
            th{text-align: center;}
        </style>
        <link href="css/bootstrap.css" rel="stylesheet" />
        <link href="dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    </head>
    <body>
        <div class="col-sm-10">
            <form action="" method="POST">
                <th>Pencarian :</th>
                <td>
                    <input type="text" name='nama_cari'>
                    <input type="submit" name="cari" value="Cari"/> 
                </td>
            </form>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" style="margin-left: auto; margin-right: auto;" id="example1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Hobi</th>
                            <th> Jumlah Person</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            if(isset($_POST['cari'])){
                                include 'soal3a.php';
                                $nama_cari = $_POST['nama_cari'];
                                $sql = mysqli_query($conection,"SELECT hobi.hobi, COUNT(person.id) AS jumlah_Person FROM person 
                                LEFT JOIN hobi ON person.id=hobi.person_id WHERE hobi.hobi LIKE '%$nama_cari' GROUP BY hobi.hobi ORDER BY jumlah_Person DESC");
                            }else{
                                // error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                                include 'soal3a.php';
                                $sql = mysqli_query($conection,"SELECT hobi.hobi, COUNT(person.id) AS jumlah_Person FROM person 
                                LEFT JOIN hobi ON person.id=hobi.person_id WHERE hobi.hobi IS NOT NULL GROUP BY hobi.hobi ORDER BY jumlah_Person DESC");
                            }
                                $no = 1;
                                while ($row = mysqli_fetch_assoc($sql)){
                            ?>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $row['hobi']; ?></td>
                                <td><?php echo $row['jumlah_Person']; ?></td>   
                        </tr> 
                        <?php 
                            }
                        ?>
                    </tbody>
                    
                </table>
            </div> 
        </div>
        <script src="js/jquery-1.12.4.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="datatables/jquery.dataTables.min.js"></script>
        <script src="datatables/dataTables.bootstrap.min.js"></script>
        <script src="datatables/pdfmake.min.js"></script>
        <script src="datatables/jszip.min.js"></script>
        <script src="datatables/buttons.print.min.js"></script>
        <script src="datatables/vfs_fonts.js"></script>
        <script src="datatables/dataTables.buttons.min.js"></script>
        <script src="datatables/buttons.html5.min.js"></script>
        <script>
        $(document).ready(function () {
            $('#example1').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                     'excel', 'pdf', 'print'
                ]
            } );
        });
    </script>
    </body>
</html>

